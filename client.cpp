#include <string>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <netdb.h>
#include <cstring>
#include <regex>
#include "network/address.h"
#include "network/poller.h"
#include "network/tcp_socket.h"
#include "network/udp_socket.h"
#include "network/messages/GUIInputMessage.h"
#include "network/messages/client_message.h"
#include "network/messages/server_message.h"
#include "network/messages/events/new_game_event.h"
#include "get_time.h"
#include "network/syserr.h"

constexpr uint64_t update_time = 20 * 1000;

static void print_usage();

void receive_gui_message(const Message &msg, int8_t *turn_direction, PollWrapper *gui_socket);

void recieve_server_message(const Message &msg, uint32_t *game_id, uint32_t *next_expected_event,
                            std::vector<std::string> *player_names, bool *running, PollWrapper *gui_socket,
                            PollWrapper *server_socket) {
    using std::placeholders::_1;
    std::cout << "rec_hand" << std::endl;
    const ServerMessage &message = *reinterpret_cast<const ServerMessage *>(&msg);
    const auto &events = message.events;
    if (message.game_id != *game_id) {
        if (events.front()->event_no != 0) {
            server_socket->queryReceive(ServerMessage(),
                                        std::bind(recieve_server_message, _1, game_id, next_expected_event,
                                                  player_names,
                                                  running, gui_socket, server_socket));
            return;
        } //Message from old game
        const auto &event = events.front();
        *game_id = message.game_id;
        *next_expected_event = 0;
        *player_names = (reinterpret_cast<NewGameEvent *>(event.get()))->player_names;
        *running = true;
    }

    std::cout << "Recieved " << events.size() << " events, next " << events.back()->event_no + 1;
    size_t i = 0;
    for (; i < events.size() && events[i]->event_no < *next_expected_event; ++i) {
        const auto &event = events[i];
        if (event->event_type == NewGameEvent::EVENT_TYPE) {
            *player_names = (reinterpret_cast<NewGameEvent *>(event.get()))->player_names;
        }
    }
    for (; i < events.size(); ++i) {
        auto gui_message = events[i]->toGUIMessage(*player_names);
        std::cout << (const char *) gui_message->toData().data() << std::endl;
        gui_socket->querySend(Address(0), std::move(gui_message));
    }
    *next_expected_event = std::max(*next_expected_event, events.back()->event_no + 1);

    server_socket->queryReceive(ServerMessage(),
                                std::bind(recieve_server_message, _1, game_id, next_expected_event, player_names,
                                          running, gui_socket, server_socket));
}

Address host_to_address(std::string hostname, int socktype, const std::string &default_port) {
    addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_CANONNAME;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = socktype;
    addrinfo *res;
    std::string port = default_port;
    auto number_of_colons = std::count(hostname.begin(), hostname.end(), ':');
    if (number_of_colons == 1 || (number_of_colons == 2 && hostname.find("::") == hostname.npos)) {
        port = std::string(hostname.begin() + hostname.find_last_of(':') + 1, hostname.end());
        hostname.resize(hostname.find_last_of(':'));
    }
    if (getaddrinfo(hostname.c_str(), port.c_str(), &hints, &res)) {
        syserr("getaddrinfo");
        print_usage();
    }
    std::cout << (res->ai_family == AF_INET) << std::endl << " "
              << ntohl(((sockaddr_in *) (res->ai_addr))->sin_addr.s_addr) << " "
              << ntohs(((sockaddr_in *) (res->ai_addr))->sin_port) << std::endl;
    Address address(*res->ai_addr);
    freeaddrinfo(res);
    return address;
}

int main(int argc, char **argv) {
    if (argc < 3 || argc > 4) {
        print_usage();
    }


    std::string player_name = argv[1];
    if (player_name.size() == 0 || player_name.size() > 64) {
        print_usage();
    }
    for (size_t i = 0; i < player_name.size(); ++i) {
        if (player_name[i] < 33 || player_name[i] > 126) {
            print_usage();
        }
    }
    std::string game_server_host = argv[2];
    Address game_server_address = host_to_address(game_server_host, SOCK_DGRAM, "12345");
    Address ui_server_address(0x7F000001, 12346);
    if (argc == 4) {
        ui_server_address = host_to_address(argv[3], SOCK_STREAM, "12346");
    }

    Poller poller;
    PollWrapper::Ptr gui_socket = std::make_shared<PollWrapper>(std::make_unique<TCPSocket>(ui_server_address));
    PollWrapper::Ptr server_socket = std::make_shared<PollWrapper>(std::make_unique<UDPSocket>(true));
    poller.addPollee(gui_socket);
    poller.addPollee(server_socket);

    int8_t turn_direction = 0;
    bool running = false;
    uint32_t next_expected_event = 0;
    std::vector<std::string> player_names;
    uint32_t game_id = 0;
    uint64_t session_id = time(NULL);


    using std::placeholders::_1;
    gui_socket->queryReceive(GUIInputMessage(), std::bind(receive_gui_message, _1, &turn_direction, gui_socket.get()));

    server_socket->queryReceive(ServerMessage(),
                                std::bind(recieve_server_message, _1, &game_id, &next_expected_event, &player_names,
                                          &running, gui_socket.get(), server_socket.get()));

    server_socket->querySend(game_server_address,
                             std::make_unique<ClientMessage>(
                                     session_id, turn_direction, next_expected_event, player_name));
    uint64_t last_time = get_current_time_in_us();
    uint64_t accumulated_us;

    while (true) {
        poller.poll();
        uint64_t current_time = get_current_time_in_us();
        accumulated_us += current_time - last_time;
        last_time = current_time;
        if (accumulated_us > update_time) {
            accumulated_us -= update_time;
            std::cout << "send " << (int) turn_direction << std::endl;
            server_socket->querySend(game_server_address,
                                     std::make_unique<ClientMessage>(
                                             session_id, turn_direction, next_expected_event, player_name));
        }
    }
}

void receive_gui_message(const Message &msg, int8_t *turn_direction, PollWrapper *gui_socket) {
    const GUIInputMessage &message = *reinterpret_cast<const GUIInputMessage *>(&msg);
    if (message.key == Key::left) {
        *turn_direction = (-1) * message.pressed;
    } else {
        *turn_direction = message.pressed;
    }

    using std::placeholders::_1;
    gui_socket->queryReceive(GUIInputMessage(), std::bind(receive_gui_message, _1, turn_direction, gui_socket));
}

static void print_usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "./siktacka-client player_name game_server_host[:port] [ui_server_host[:port]]" << std::endl;
    exit(1);
}