//
// Created by jarek on 09.06.17.
//

#include <cstdlib>
#include <sys/time.h>
#include "get_time.h"

uint64_t get_current_time_in_us() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return static_cast<uint64_t>(tv.tv_sec) * 1000000 + tv.tv_usec;
}