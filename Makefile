.PHONY: all server client

all: server client

server:
	g++ -std=c++14 -O2 -o siktacka-server  network/udp_socket.cpp network/syserr.cpp network/address.cpp  network/poller.cpp  network/poll_wrapper.cpp network/messages/server_message.cpp network/messages/event.cpp network/messages/pack.cpp network/messages/events/new_game_event.cpp network/crc.cpp network/messages/events/pixel_event.cpp network/messages/events/player_eliminated_event.cpp network/messages/events/game_over_event.cpp network/messages/client_message.cpp network/messages/GUINewGameMessage.cpp network/messages/GUIPixelMessage.cpp network/messages/GUIPlayerEliminatedMessage.cpp get_time.cpp  server.cpp server/rand.cpp  server/Client.cpp  server/player.cpp  server/game_state.cpp  server/game_server.cpp  server/trigonometry.cpp 

client:
	g++ -std=c++14 -O2 -o siktacka-client network/udp_socket.cpp network/syserr.cpp network/address.cpp  network/poller.cpp  network/poll_wrapper.cpp network/messages/server_message.cpp network/messages/event.cpp network/messages/pack.cpp network/messages/events/new_game_event.cpp network/crc.cpp network/messages/events/pixel_event.cpp network/messages/events/player_eliminated_event.cpp network/messages/events/game_over_event.cpp network/messages/client_message.cpp network/messages/GUINewGameMessage.cpp network/messages/GUIPixelMessage.cpp network/messages/GUIPlayerEliminatedMessage.cpp get_time.cpp network/tcp_socket.cpp  network/messages/GUIInputMessage.cpp  client.cpp
