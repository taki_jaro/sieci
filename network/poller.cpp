//
// Created by jarek on 05.06.17.
//

#include <algorithm>
#include "poller.h"
#include "syserr.h"


void Poller::poll() {
    std::vector<pollfd> pollfds;
    std::transform(pollees.begin(), pollees.end(), std::back_inserter(pollfds),
                  [](std::shared_ptr<PollWrapper> pollee){ return pollee->getPollFd(); });

    int ret = ::poll(pollfds.data(), pollfds.size(), 0);
    if (ret < 0) {
        syserr("poll");
    }

    if (ret == 0) {
        return;
    }


    for (size_t i = 0; i < pollfds.size(); ++i) {
        processPollEvents(pollfds[i].revents, pollees[i]);
    }
}

void Poller::addPollee(std::shared_ptr<PollWrapper> pollee) {
    pollees.push_back(pollee);
}

void Poller::removePollee(std::shared_ptr<PollWrapper> pollee) {
    std::remove(pollees.begin(), pollees.end(), pollee);
}

void Poller::processPollEvents(const short events, std::shared_ptr<PollWrapper> pollee) {
    if (events & (POLLIN | POLLERR)) {
        pollee->doReceive();
    }
    if (events & POLLOUT) {
        pollee->doSend();
    }
}
