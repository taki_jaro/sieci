//
// Created by jarek on 05.06.17.
//

#include "poll_wrapper.h"

PollWrapper::PollWrapper(std::unique_ptr<Socket> socket) : socket(std::move(socket)) {

}

void PollWrapper::querySend(Address address, std::unique_ptr<Message> message) {
    send_queue.emplace(address, std::move(message));
}

void PollWrapper::queryReceive(const Message &message, std::function<void(const Message &)> callback) {
    receive_queue.emplace(message.emptyMessage(), callback);
}

pollfd PollWrapper::getPollFd() const {
    pollfd poll;
    poll.revents = 0;
    poll.fd = socket->getPollingFd();
    poll.events = POLLERR;

    if (!send_queue.empty()) {
        poll.events |= POLLOUT;
    }
    if (!receive_queue.empty()) {
        poll.events |= POLLIN;
    }
    return poll;
}

void PollWrapper::doSend() {
    auto command = std::move(send_queue.front());
    send_queue.pop();
    Address address = command.first;
    const Message &message = *command.second;
    socket->sendTo(address, message);
}

void PollWrapper::doReceive() {
    auto command = std::move(receive_queue.front());
    receive_queue.pop();

    Message &message = *command.first.get();
    auto callback = command.second;
    socket->receive(message);
    callback(message);
}
