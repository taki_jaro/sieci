//
// Created by jarek on 09.06.17.
//

#include "tcp_socket.h"
#include "syserr.h"
#include "../consts.h"
#include <unistd.h>
#include <cstring>
#include <netinet/tcp.h>

TCPSocket::TCPSocket(Address address) {
    auto addr = address.toSockaddr();
    socket_fd = socket(addr->sa_family, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        syserr("socket");
    }

    if (connect(socket_fd, addr.get(), sizeof(sockaddr))) {
        syserr("connect");
    }
    int i = 1;
    setsockopt(socket_fd, IPPROTO_TCP, TCP_NODELAY, (void *)&i, sizeof(i));
}

void TCPSocket::sendTo(Address, const Message &message) {
    auto data = message.toData();
    if (write(socket_fd, data.data(), data.size()) != data.size()) {
        syserr("TCP failed/partial write");
    }
}

void TCPSocket::receive(Message &message) {
    std::vector<uint8_t> buffer;
    buffer.resize(MAX_PACKET_SIZE);

    ssize_t received = read(socket_fd, buffer.data(), buffer.size());
    if (received < 0) {
        syserr("read");
    }
    buffer.resize(static_cast<size_t>(received));
    message.parse(buffer);
}

int TCPSocket::getPollingFd() const {
    return socket_fd;
}
