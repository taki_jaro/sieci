#ifndef ZAD_ADDRESS_H
#define ZAD_ADDRESS_H


#include <cstdint>
#include <netinet/in.h>
#include <memory>




class Address {
public:
    Address(uint32_t ip, uint16_t port);
    Address(uint16_t port);
    Address(in6_addr ipv6, uint16_t port);
    Address(const sockaddr &addr);

    std::unique_ptr<sockaddr> toSockaddr();

    bool isIPv6();

    bool operator<(const Address &rhs) const;

private:
    uint32_t ip;
    uint16_t port;
    in6_addr ipv6;
    bool is_ipv6;
};


#endif //ZAD_ADDRESS_H
