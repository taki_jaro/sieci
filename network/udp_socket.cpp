//
// Created by jarek on 04.06.17.
//

#include <iostream>
#include <arpa/inet.h>
#include "udp_socket.h"
#include "syserr.h"
#include "../consts.h"

UDPSocket::UDPSocket(Address address) {
    if (address.isIPv6()) {
        socket_fd = socket(AF_INET6, SOCK_DGRAM, 0);
    } else {
        socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    }

    if (socket_fd < 0) {
        syserr("socket");
    }


    auto p = address.toSockaddr();
    if (bind(socket_fd, (sockaddr*)p.get(), sizeof(sockaddr_in6)) < 0) {
        syserr("bind");
    }
}

UDPSocket::UDPSocket(bool ipv6) {
    if (ipv6) {
        socket_fd = socket(AF_INET6, SOCK_DGRAM, 0);
    } else {
        socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    }

    if (socket_fd < 0) {
        syserr("socket");
    }
}


void UDPSocket::sendTo(Address address, const Message &message) {
    auto data = message.toData();
    if (sendto(socket_fd, data.data(), data.size(), 0,
               (sockaddr *) address.toSockaddr().get(), sizeof(sockaddr_in6)) != data.size()) {
        syserr("Falied/partial write");
    }
    std::cout << "sending" << std::endl;
}

void UDPSocket::receive(Message &message) {
    std::vector<uint8_t> buffer;
    buffer.resize(MAX_PACKET_SIZE);

    sockaddr addr;
    socklen_t size = sizeof(addr);
    auto received = recvfrom(socket_fd, buffer.data(), buffer.capacity(), 0, &addr, &size);
    if (received < 0) {
        syserr("recvfrom");
    }

    std::cout << "receiving" << std::endl;

    buffer.resize(static_cast<size_t >(received));
    message.parse(buffer);
    message.from = Address(addr);
}


int UDPSocket::getPollingFd() const {
    return socket_fd;
}



