//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_POLLER_H
#define ZAD_POLLER_H

#include <list>
#include <memory>
#include "poll_wrapper.h"

class Poller {
public:
    void poll();
    void addPollee(std::shared_ptr<PollWrapper> pollee);
    void removePollee(std::shared_ptr<PollWrapper> pollee);

private:
    void processPollEvents(const short events, std::shared_ptr<PollWrapper> pollee);

    std::vector<std::shared_ptr<PollWrapper> > pollees;
};


#endif //ZAD_POLLER_H
