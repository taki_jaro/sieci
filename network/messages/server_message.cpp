//
// Created by jarek on 05.06.17.
//

#include <cassert>
#include <netinet/in.h>
#include <algorithm>
#include "server_message.h"
#include "pack.h"
#include "../../consts.h"

void ServerMessage::parse(const std::vector<uint8_t> &data) {
    assert(data.size() > 4);
    uint8_t *buf = (uint8_t*) data.data();
    game_id = unpack_u32(&buf);
    while (buf - data.data() < data.size()) {
        events.push_back(Event::parseEvent(&buf));
        if (!events.back()) {
            events.pop_back();
            break;
        }
    }
}

std::vector<uint8_t> ServerMessage::toData() const {
    std::vector<uint8_t> data;
    data.resize(MAX_PACKET_SIZE);
    uint8_t *buf = (uint8_t*) data.data();

    pack_u32(game_id, &buf);
    std::for_each(events.begin(), events.end(),
                    [&buf](Event::Ptr event) {event->toData(&buf);});

    data.resize(buf - data.data());
    return data;
}

std::unique_ptr<Message> ServerMessage::emptyMessage() const {
    return std::make_unique<ServerMessage>();
}
