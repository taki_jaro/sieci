//
// Created by jarek on 09.06.17.
//

#include <cassert>
#include "GUIInputMessage.h"

void GUIInputMessage::parse(const std::vector<uint8_t> &data) {
    std::string str = (const char *)data.data();

    if (str.find("LEFT") != str.npos) {
        key = Key::left;
    } else {
        key = Key::right;
    }

    pressed = str.find("UP") == str.npos;
}

std::vector<uint8_t> GUIInputMessage::toData() const {
    assert(false) ;      //We do not plan to send this
    return {};
}

std::unique_ptr<Message> GUIInputMessage::emptyMessage() const {
    return std::make_unique<GUIInputMessage>();
}
