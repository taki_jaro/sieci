//
// Created by jarek on 09.06.17.
//

#include <cassert>
#include <sstream>
#include "GUIPixelMessage.h"

GUIPixelMessage::GUIPixelMessage(uint32_t x, uint32_t y, const std::string &player_name) : x(x), y(y),
                                                                                           player_name(player_name) {}

void GUIPixelMessage::parse(const std::vector<uint8_t> &data) {
    assert(false); //We do not plan to receive this message
}

std::vector<uint8_t> GUIPixelMessage::toData() const {
    std::ostringstream stream;
    stream << "PIXEL " << x << " " << y << " " << player_name << "\n";
    std::string string = stream.str();
    return std::vector<uint8_t > (string.begin(), string.end());
}

std::unique_ptr<Message> GUIPixelMessage::emptyMessage() const {
    assert(false); //We do not plan to receive this message
    return nullptr;
}
