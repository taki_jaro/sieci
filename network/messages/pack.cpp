//
// Created by jarek on 05.06.17.
//

#include <netinet/in.h>
#include "pack.h"

#if defined(__linux__)
#  include <endian.h>
#elif defined(__FreeBSD__) || defined(__NetBSD__)
#  include <sys/endian.h>
#elif defined(__OpenBSD__)
#  include <sys/types.h>
#  define be16toh(x) betoh16(x)
#  define be32toh(x) betoh32(x)
#  define be64toh(x) betoh64(x)
#endif

void pack_i8(int8_t val, uint8_t **buf) {
    uint8_t byte = *reinterpret_cast<uint8_t *>(&val);
    **buf = byte;
    ++*buf;
}

void pack_u8(uint8_t val, uint8_t **buf) {
    **buf = val;
    ++*buf;
}

void pack_u32(uint32_t val, uint8_t **buf) {
    uint32_t network = htonl(val);
    uint8_t *bytes = reinterpret_cast<uint8_t *>(&network);

    for (int i = 0; i < 4; ++i) {
        **buf = bytes[i];
        ++*buf;
    }
}

void pack_u64(uint64_t val, uint8_t **buf) {
    uint64_t network = htobe64(val);
    uint8_t *bytes = reinterpret_cast<uint8_t *>(&network);

    for (int i = 0; i < 8; ++i) {
        **buf = bytes[i];
        ++*buf;
    }

}

void pack_string(const std::string val, uint8_t **buf, bool append_zero_byte) {
    for (char c : val) {
        **buf = *reinterpret_cast<uint8_t *>(&c);
        ++*buf;
    }

    if (append_zero_byte) {
        *((*buf)++) = 0;
    }
}


int8_t unpack_i8(uint8_t **buf) {
    int8_t ret = *reinterpret_cast<int8_t *>(*buf);
    ++*buf;
    return ret;
}

uint8_t unpack_u8(uint8_t **buf) {
    return *((*buf)++);
}

uint32_t unpack_u32(uint8_t **buf) {
    uint32_t network = *reinterpret_cast<uint32_t *>(*buf);
    *buf += 4;
    return ntohl(network);
}

uint64_t unpack_u64(uint8_t **buf) {
    uint64_t network = *reinterpret_cast<uint64_t *>(*buf);
    *buf += 8;
    return be64toh(network);
}

std::string unpack_string(uint8_t **buf) {
    std::string res;
    while (**buf) {
        res.push_back(**buf);
        ++*buf;
    }
    ++*buf;
    return res;
}

std::string unpack_string(uint8_t **buf, size_t size) {
    std::string str;
    str.reserve(size);
    for (size_t i = 0; i < size; ++i) {
        str.push_back(**buf);
        ++*buf;
    }
    return str;
}



