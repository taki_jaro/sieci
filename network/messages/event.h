//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_EVENT_H
#define ZAD_EVENT_H


#include <cstdint>
#include <memory>
#include "../message.h"

class Event {
public:
    using Ptr = std::shared_ptr<Event>;

    static Ptr parseEvent(uint8_t **buf);

    virtual ~Event(){}
    uint32_t getEventLength();
    void toData(uint8_t **buf);
    void parse(uint8_t **buf);

    virtual std::unique_ptr<Message> toGUIMessage(const std::vector<std::string> &player_names) = 0;

    uint32_t len;
    uint32_t event_no;
    uint8_t event_type;
    uint32_t crc;

protected:
    static constexpr uint32_t EVENT_LEN = sizeof(event_type) + sizeof(event_no);

    virtual void writeEventData(uint8_t **buf) = 0;
    virtual void parseEventData(uint8_t **buf) = 0;
    virtual uint32_t getEventDataLen() = 0;

};


#endif //ZAD_EVENT_H
