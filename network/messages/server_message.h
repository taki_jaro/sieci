//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_SERVERMESSAGE_H
#define ZAD_SERVERMESSAGE_H


#include "../message.h"
#include "event.h"

class ServerMessage : public Message {
public:
    void parse(const std::vector<uint8_t> &data) override;

    std::vector<uint8_t> toData() const override;

    std::unique_ptr<Message> emptyMessage() const override;

    uint32_t game_id;
    std::vector<Event::Ptr> events;
};


#endif //ZAD_SERVERMESSAGE_H
