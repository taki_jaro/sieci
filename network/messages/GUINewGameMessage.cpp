//
// Created by jarek on 09.06.17.
//

#include <cassert>
#include <sstream>
#include "GUINewGameMessage.h"

GUINewGameMessage::GUINewGameMessage(uint32_t maxx, uint32_t maxy, const std::vector<std::string> &player_names) : maxx(
        maxx), maxy(maxy), player_names(player_names) {}

void GUINewGameMessage::parse(const std::vector<uint8_t> &data) {
    assert(false); //We do not plan to receive this message
}

std::vector<uint8_t> GUINewGameMessage::toData() const {
    std::ostringstream stream;
    stream << "NEW_GAME " << maxx << " " << maxy;
    for (const auto &name : player_names) {
        stream << " " << name;
    }
    stream << "\n";
    std::string string = stream.str();
    return std::vector<uint8_t>(string.begin(), string.end());
}

std::unique_ptr<Message> GUINewGameMessage::emptyMessage() const {
    assert(false); //We do not plan to receive this message
    return nullptr;
}
