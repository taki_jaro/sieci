//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_PACK_H
#define ZAD_PACK_H

#include <cstdint>
#include <string>

void pack_i8(int8_t val, uint8_t **buf);
void pack_u8(uint8_t val, uint8_t **buf);
void pack_u32(uint32_t val, uint8_t **buf);
void pack_u64(uint64_t val, uint8_t **buf);
void pack_string(const std::string val, uint8_t **buf, bool append_zero_byte = true);

int8_t unpack_i8(uint8_t **buf);
uint8_t unpack_u8(uint8_t **buf);
uint32_t unpack_u32(uint8_t **buf);
uint64_t unpack_u64(uint8_t **buf);
std::string unpack_string(uint8_t **buf);
std::string unpack_string(uint8_t **buf, size_t size);

#endif //ZAD_PACK_H
