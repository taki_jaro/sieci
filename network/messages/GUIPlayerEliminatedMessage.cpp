//
// Created by jarek on 09.06.17.
//

#include <cassert>
#include <sstream>
#include "GUIPlayerEliminatedMessage.h"

GUIPlayerEliminatedMessage::GUIPlayerEliminatedMessage(const std::string &player_name) : player_name(player_name) {}

void GUIPlayerEliminatedMessage::parse(const std::vector<uint8_t> &data) {
    assert(false); //We do not plan to receive this message
}

std::vector<uint8_t> GUIPlayerEliminatedMessage::toData() const {
    std::ostringstream stream;
    stream << "PLAYER_ELIMINATED " << player_name << "\n";
    std::string string = stream.str();
    return std::vector<uint8_t>(string.begin(), string.end());
}

std::unique_ptr<Message> GUIPlayerEliminatedMessage::emptyMessage() const {
    assert(false); //We do not plan to receive this message
    return nullptr;
}
