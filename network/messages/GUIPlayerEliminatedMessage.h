//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GUIPLAYERELIMINATEDMESSAGE_H
#define ZAD_GUIPLAYERELIMINATEDMESSAGE_H


#include "../message.h"

class GUIPlayerEliminatedMessage : public Message {
public:
    GUIPlayerEliminatedMessage(const std::string &player_name);

    void parse(const std::vector<uint8_t> &data) override;

    std::unique_ptr<Message> emptyMessage() const override;

    std::vector<uint8_t> toData() const override;

private:
    std::string player_name;
};


#endif //ZAD_GUIPLAYERELIMINATEDMESSAGE_H
