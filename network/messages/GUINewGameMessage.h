//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GUINEWGAMEMESSAGE_H
#define ZAD_GUINEWGAMEMESSAGE_H


#include "../message.h"

class GUINewGameMessage : public Message {
public:
    GUINewGameMessage(uint32_t maxx, uint32_t maxy, const std::vector<std::string> &player_names);

    void parse(const std::vector<uint8_t> &data) override;

    std::vector<uint8_t> toData() const override;

    std::unique_ptr<Message> emptyMessage() const override;

private:
    uint32_t maxx, maxy;
    std::vector<std::string> player_names;
};


#endif //ZAD_GUINEWGAMEMESSAGE_H
