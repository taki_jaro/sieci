//
// Created by jarek on 06.06.17.
//

#include "client_message.h"
#include "pack.h"
#include "../../consts.h"

void ClientMessage::parse(const std::vector<uint8_t> &data) {
    uint8_t *buf = (uint8_t*) data.data();
    session_id = unpack_u64(&buf);
    turn_direction = unpack_i8(&buf);
    next_expected_event_no = unpack_u32(&buf);
    player_name = unpack_string(&buf, data.size() - (buf - data.data()));
}


std::vector<uint8_t> ClientMessage::toData() const {
    std::vector<uint8_t> data;
    data.resize(MAX_PACKET_SIZE);

    uint8_t *buf = data.data();
    pack_u64(session_id, &buf);
    pack_i8(turn_direction, &buf);
    pack_u32(next_expected_event_no, &buf);
    pack_string(player_name, &buf, false);

    data.resize(buf - data.data());
    return data;
}

std::unique_ptr<Message> ClientMessage::emptyMessage() const {
    return std::make_unique<ClientMessage>();
}

ClientMessage::ClientMessage(uint64_t session_id, int8_t turn_direction, uint32_t next_expected_event_no,
                             const std::string &player_name) : session_id(session_id), turn_direction(turn_direction),
                                                               next_expected_event_no(next_expected_event_no),
                                                               player_name(player_name) {}
