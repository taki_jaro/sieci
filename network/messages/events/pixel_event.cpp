//
// Created by jarek on 05.06.17.
//

#include "pixel_event.h"
#include "../pack.h"
#include "../GUIPixelMessage.h"

void PixelEvent::writeEventData(uint8_t **buf) {
    pack_u8(player_number, buf);
    pack_u32(x, buf);
    pack_u32(y, buf);
}

void PixelEvent::parseEventData(uint8_t **buf) {
    player_number = unpack_u8(buf);
    x = unpack_u32(buf);
    y = unpack_u32(buf);
}

uint32_t PixelEvent::getEventDataLen() {
    return sizeof(player_number) + sizeof(x) + sizeof(y);
}

PixelEvent::PixelEvent()  {
    event_type = EVENT_TYPE;
}

std::unique_ptr<Message> PixelEvent::toGUIMessage(const std::vector<std::string> &player_names) {
    return std::make_unique<GUIPixelMessage>(x, y, player_names[player_number]);
}

