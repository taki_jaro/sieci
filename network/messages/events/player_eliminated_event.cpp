//
// Created by jarek on 05.06.17.
//

#include "player_eliminated_event.h"
#include "../pack.h"
#include "../GUIPlayerEliminatedMessage.h"

void PlayerEliminatedEvent::writeEventData(uint8_t **buf) {
    pack_u8(player_number, buf);
}

void PlayerEliminatedEvent::parseEventData(uint8_t **buf) {
    player_number = unpack_u8(buf);
}

uint32_t PlayerEliminatedEvent::getEventDataLen() {
    return sizeof(player_number);

}

PlayerEliminatedEvent::PlayerEliminatedEvent()  {
    event_type = EVENT_TYPE;
}

std::unique_ptr<Message> PlayerEliminatedEvent::toGUIMessage(const std::vector<std::string> &player_names) {
    return std::make_unique<GUIPlayerEliminatedMessage>(player_names[player_number]);
}
