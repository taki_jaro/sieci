//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_PLAYERELIMINATEDEVENT_H
#define ZAD_PLAYERELIMINATEDEVENT_H


#include <cstdint>
#include "../event.h"

class PlayerEliminatedEvent : public Event {
public:
    static constexpr uint8_t EVENT_TYPE = 2;

    PlayerEliminatedEvent();

    std::unique_ptr<Message> toGUIMessage(const std::vector<std::string> &player_names) override;

    uint8_t player_number;

protected:
    void parseEventData(uint8_t **buf) override;
    uint32_t getEventDataLen() override;

    void writeEventData(uint8_t **buf) override;
};


#endif //ZAD_PLAYERELIMINATEDEVENT_H
