//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_NEWGAMEEVENT_H
#define ZAD_NEWGAMEEVENT_H


#include <vector>
#include <string>
#include "../event.h"

class NewGameEvent : public Event {
public:
    static constexpr uint8_t EVENT_TYPE = 0;

    NewGameEvent();

    std::unique_ptr<Message> toGUIMessage(const std::vector<std::string> &player_names) override;

    uint32_t maxx, maxy;
    std::vector<std::string> player_names;
protected:
    void writeEventData(uint8_t **buf) override;

    uint32_t getEventDataLen() override;

    void parseEventData(uint8_t **buf) override;
};


#endif //ZAD_NEWGAMEEVENT_H
