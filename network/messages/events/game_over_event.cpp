//
// Created by jarek on 05.06.17.
//

#include "game_over_event.h"
#include "../client_message.h"

void GameOverEvent::writeEventData(uint8_t **) {

}

void GameOverEvent::parseEventData(uint8_t **) {

}

uint32_t GameOverEvent::getEventDataLen() {
    return 0;
}

GameOverEvent::GameOverEvent() {
    event_type = EVENT_TYPE;
}

std::unique_ptr<Message> GameOverEvent::toGUIMessage(const std::vector<std::string> &) {
    return nullptr; //This do not have correspondent GUI message, will get ignored later
}
