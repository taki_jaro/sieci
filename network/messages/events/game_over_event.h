//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_GAMEOVEREVENT_H
#define ZAD_GAMEOVEREVENT_H


#include "../event.h"

class GameOverEvent : public Event {
public:
    static constexpr uint8_t EVENT_TYPE = 3;

    GameOverEvent();

    std::unique_ptr<Message> toGUIMessage(const std::vector<std::string> &player_names) override;

protected:
    void writeEventData(uint8_t **) override;
    uint32_t getEventDataLen() override;
    void parseEventData(uint8_t **) override;
};


#endif //ZAD_GAMEOVEREVENT_H
