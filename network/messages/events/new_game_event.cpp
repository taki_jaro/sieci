//
// Created by jarek on 05.06.17.
//

#include <algorithm>
#include <numeric>
#include "new_game_event.h"
#include "../pack.h"
#include "../GUINewGameMessage.h"

void NewGameEvent::writeEventData(uint8_t **buf) {
    pack_u32(maxx, buf);
    pack_u32(maxy, buf);
    std::for_each(player_names.begin(), player_names.end(),
                    [&buf](const std::string &str){ pack_string(str, buf);});
}

void NewGameEvent::parseEventData(uint8_t **buf) {
    const uint8_t *start = *buf;
    maxx = unpack_u32(buf);
    maxy = unpack_u32(buf);
    while (*buf - start + EVENT_LEN < len) {
        player_names.push_back(unpack_string(buf));
    }
}

uint32_t NewGameEvent::getEventDataLen() {
    return sizeof(maxx) + sizeof(maxy) +
            std::accumulate(player_names.begin(), player_names.end(), 0,
                            [](uint32_t acc, const std::string &str) { return acc + str.size()+1;});
}

NewGameEvent::NewGameEvent()  {
    event_type = EVENT_TYPE;
}

std::unique_ptr<Message> NewGameEvent::toGUIMessage(const std::vector<std::string> &) {
    return std::make_unique<GUINewGameMessage>(maxx,maxy, player_names);
}

