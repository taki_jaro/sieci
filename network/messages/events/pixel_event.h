//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_PIXELEVENT_H
#define ZAD_PIXELEVENT_H


#include "../event.h"

class PixelEvent : public Event {
public:
    static constexpr uint8_t EVENT_TYPE = 1;

    PixelEvent();

    std::unique_ptr<Message> toGUIMessage(const std::vector<std::string> &player_names) override;

    uint8_t player_number;
    uint32_t x, y;

protected:
    void writeEventData(uint8_t **buf) override;
    void parseEventData(uint8_t **buf) override;
    uint32_t getEventDataLen() override;
};


#endif //ZAD_PIXELEVENT_H
