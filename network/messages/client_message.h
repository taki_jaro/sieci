//
// Created by jarek on 06.06.17.
//

#ifndef ZAD_CLIENTMESSAGE_H
#define ZAD_CLIENTMESSAGE_H

#include <string>
#include "../message.h"

class ClientMessage : public Message {
public:
    ClientMessage(){}
    ClientMessage(uint64_t session_id, int8_t turn_direction, uint32_t next_expected_event_no,
                  const std::string &player_name);

    std::vector<uint8_t> toData() const override;

    void parse(const std::vector<uint8_t> &data) override;

    uint64_t session_id;
    int8_t turn_direction;
    uint32_t next_expected_event_no;
    std::string player_name;

    std::unique_ptr<Message> emptyMessage() const override;
};


#endif //ZAD_CLIENTMESSAGE_H
