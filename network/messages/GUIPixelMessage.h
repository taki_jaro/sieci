//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GUIPIXELMESSAGE_H
#define ZAD_GUIPIXELMESSAGE_H


#include "../message.h"

class GUIPixelMessage : public Message {
public:
    GUIPixelMessage(uint32_t x, uint32_t y, const std::string &player_name);

    void parse(const std::vector<uint8_t> &data) override;

    std::vector<uint8_t> toData() const override;

    std::unique_ptr<Message> emptyMessage() const override;

private:
    uint32_t x, y;
    std::string player_name;
};


#endif //ZAD_GUIPIXELMESSAGE_H
