//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GUIINPUTMESSAGE_H
#define ZAD_GUIINPUTMESSAGE_H


#include "../message.h"

enum class Key {
    left, right
};

class GUIInputMessage : public Message {
public:
    void parse(const std::vector<uint8_t> &data) override;

    std::unique_ptr<Message> emptyMessage() const override;

    std::vector<uint8_t> toData() const override;

    Key key;
    bool pressed;
};


#endif //ZAD_GUIINPUTMESSAGE_H
