//
// Created by jarek on 05.06.17.
//

#include "event.h"
#include "pack.h"
#include "../crc.h"
#include "events/new_game_event.h"
#include "events/pixel_event.h"
#include "events/player_eliminated_event.h"
#include "events/game_over_event.h"

void Event::toData(uint8_t **buf) {
    len = getEventLength();
    uint8_t *start = *buf;
    pack_u32(len, buf);
    pack_u32(event_no, buf);
    pack_u8(event_type, buf);
    writeEventData(buf);
    crc = crc32buf((char*)start, *buf - start);
    pack_u32(crc, buf);
}

uint32_t Event::getEventLength() { return EVENT_LEN + getEventDataLen(); }

void Event::parse(uint8_t **buf) {
    len = unpack_u32(buf);
    event_no = unpack_u32(buf);
    event_type = unpack_u8(buf);
    parseEventData(buf);
    crc = unpack_u32(buf);
}

Event::Ptr Event::parseEvent(uint8_t **buf) {
    uint8_t type = 0;
    Ptr event;
    do {
        uint8_t *tmp = *buf;
        type = tmp[sizeof(len) + sizeof(event_no)];

        uint32_t len = unpack_u32(&tmp);
        tmp += len;
        uint32_t crc = unpack_u32(&tmp);
        uint32_t real_crc = crc32buf((char*)*buf, len + sizeof(len));
        if (crc != real_crc) {
            return Ptr();
        }

        switch (type) {
            case 0:
                event = std::make_shared<NewGameEvent>();
                break;
            case 1:
                event = std::make_shared<PixelEvent>();
                break;
            case 2:
                event = std::make_shared<PlayerEliminatedEvent>();
                break;
            case 3:
                event = std::make_shared<GameOverEvent>();
                break;
            default:
                *buf += sizeof(len) + len + sizeof(crc);
                break;
        }
    }while(type > 3);
    event->parse(buf);
    return event;
}
