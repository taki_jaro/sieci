//
// Created by jarek on 04.06.17.
//

#ifndef ZAD_MESSAGE_H
#define ZAD_MESSAGE_H


#include <stdint.h>
#include <vector>
#include <memory>
#include "address.h"

class Message {
public:
    virtual void parse(const std::vector<uint8_t> &data) = 0;
    virtual std::vector<uint8_t> toData() const = 0;
    virtual ~Message(){}
    virtual std::unique_ptr<Message> emptyMessage() const = 0;

    Address from = Address(0);
};


#endif //ZAD_MESSAGE_H
