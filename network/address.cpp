//
// Created by jarek on 04.06.17.
//

#include <netinet/in.h>
#include <tuple>
#include "address.h"

Address::Address(uint32_t ip, uint16_t port)
        : ip(htonl(ip)), port(htons(port)), is_ipv6(false) {

}

Address::Address(uint16_t port) : ip(INADDR_ANY),port(htons(port)), is_ipv6(false) {}

Address::Address(in6_addr ipv6, uint16_t port)
        : port(port), ipv6(ipv6), is_ipv6(true) {
}

std::unique_ptr<sockaddr> Address::toSockaddr() {
    std::unique_ptr<sockaddr> address = std::make_unique<sockaddr>();
    if (is_ipv6) {
        auto in6_address = (sockaddr_in6 *) (address.get());
        in6_address->sin6_family = AF_INET6;
        in6_address->sin6_addr = ipv6;
        in6_address->sin6_port = port;
    } else {
        auto in_address = (sockaddr_in *) address.get();
        in_address->sin_family = AF_INET;
        in_address->sin_addr.s_addr = ip;
        in_address->sin_port = port;
    }
    return address;
}

bool Address::isIPv6() {
    return is_ipv6;
}

Address::Address(const sockaddr &addr) {
    if (addr.sa_family == AF_INET) {
        sockaddr_in ipv4addr = *reinterpret_cast<const sockaddr_in *>(&addr);
        is_ipv6 = false;
        ip = ipv4addr.sin_addr.s_addr;
        port = ipv4addr.sin_port;
    } else { // addr.sa_family == AF_INET6
        sockaddr_in6 ipv6addr = *reinterpret_cast<const sockaddr_in6 *>(&addr);
        ipv6 = ipv6addr.sin6_addr;
        port = ipv6addr.sin6_port;
        is_ipv6 = true;
    }
}

bool operator<(const in6_addr &lhs, const in6_addr &rhs) {
    return std::lexicographical_compare(lhs.__in6_u.__u6_addr8, lhs.__in6_u.__u6_addr8 + 16,
                                        rhs.__in6_u.__u6_addr8, rhs.__in6_u.__u6_addr8 + 16);
}

bool Address::operator<(const Address &rhs) const {
    return std::tie(ip, port, ipv6, is_ipv6) < std::tie(rhs.ip, rhs.port, rhs.ipv6, rhs.is_ipv6);
}
