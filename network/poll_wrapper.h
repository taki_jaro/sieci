//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_POLLWRAPPER_H
#define ZAD_POLLWRAPPER_H


#include <poll.h>
#include <functional>
#include <queue>
#include <memory>
#include "address.h"
#include "message.h"
#include "socket.h"

class PollWrapper {
public:
    using Ptr = std::shared_ptr<PollWrapper>;

    PollWrapper(std::unique_ptr<Socket> socket);

    void querySend(Address address, std::unique_ptr<Message> message);
    void queryReceive(const Message &message, std::function<void(const Message &)> callback);

    pollfd getPollFd() const;
    void doSend();
    void doReceive();

private:
    using SendCommand = std::pair<Address, std::unique_ptr<Message> >;
    using ReceiveCommand = std::pair<std::unique_ptr<Message>, std::function<void(const Message &)> >;
    std::queue<SendCommand> send_queue;
    std::queue<ReceiveCommand> receive_queue;
    std::unique_ptr<Socket> socket;
};


#endif //ZAD_POLLWRAPPER_H
