//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_TCPSOCKET_H
#define ZAD_TCPSOCKET_H


#include "socket.h"

class TCPSocket : public Socket {
public:
    TCPSocket(Address address);

    void sendTo(Address address, const Message &message) override;

    void receive(Message &message) override;

    int getPollingFd() const override;

private:
    int socket_fd;

};


#endif //ZAD_TCPSOCKET_H
