//
// Created by jarek on 04.06.17.
//

#ifndef ZAD_UDP_SOCKET_H
#define ZAD_UDP_SOCKET_H


#include "address.h"
#include "message.h"
#include "socket.h"

class UDPSocket : public Socket {
public:
    UDPSocket(Address address);

    UDPSocket(bool ipv6);

    void sendTo(Address address, const Message &message) override;
    void receive(Message &message) override;

    int getPollingFd() const override;

private:
    int socket_fd;
};


#endif //ZAD_UDP_SOCKET_H
