//
// Created by jarek on 04.06.17.
//

#ifndef ZAD_SYSERR_H
#define ZAD_SYSERR_H

void syserr(const char *fmt, ...);

#endif //ZAD_SYSERR_H
