//
// Created by jarek on 05.06.17.
//

#ifndef ZAD_SOCKET_H
#define ZAD_SOCKET_H


#include <functional>
#include "address.h"
#include "message.h"

class Socket {
public:
    virtual ~Socket() { }

    virtual int getPollingFd() const = 0;
    virtual void sendTo(Address address, const Message &message) = 0;
    virtual void receive(Message &message) = 0;

};


#endif //ZAD_SOCKET_H
