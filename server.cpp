//
// Created by jarek on 06.06.17.
//

#include <cstdint>
#include <cstdio>
#include <ctime>
#include <functional>
#include <string>
#include <getopt.h>
#include <sstream>
#include <cassert>
#include <unordered_map>
#include <iostream>
#include "server/rand.h"
#include "network/poll_wrapper.h"
#include "network/udp_socket.h"
#include "network/poller.h"
#include "network/messages/event.h"
#include "server/game_state.h"
#include "network/messages/server_message.h"
#include "network/messages/client_message.h"
#include "consts.h"
#include "server/game_server.h"
#include "server/trigonometry.h"


void receive_message(const Message &msg, PollWrapper *client_socket, GameState *game_state, GameServer *server,
                     const std::vector<Event::Ptr> *events) {
    const ClientMessage &message = (ClientMessage &) msg;
    std::cout << "Receive from " << message.player_name << " next_event = " << message.next_expected_event_no << std::endl;
    Address address = message.from;
    server->updateClientStatus(address, message.session_id, message.player_name, message.next_expected_event_no);
    game_state->update_player_move(message.player_name, message.turn_direction);

    server->sendEventsTo(address, *events, *client_socket);
    using namespace std::placeholders;
    client_socket->queryReceive(ClientMessage(), std::bind(receive_message, _1, client_socket, game_state, server, events));
}


static void print_usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << " ./siktacka-server [-W n] [-H n] [-p n] [-s n] [-t n] [-r n]\n"
            "\n"
            "  -W n – szerokość planszy w pikselach (domyślnie 800)\n"
            "  -H n – wysokość planszy w pikselach (domyślnie 600)\n"
            "  -p n – numer portu (domyślnie 12345)\n"
            "  -s n – liczba całkowita wyznaczająca szybkość gry (parametr\n"
            "          ROUNDS_PER_SEC w opisie protokołu, domyślnie 50)\n"
            "  -t n – liczba całkowita wyznaczająca szybkość skrętu (parametr\n"
            "          TURNING_SPEED, domyślnie 6)\n"
            "  -r n – ziarno generatora liczb losowych" << std::endl;
    exit(1);
}

int main(int argc, char **argv) {
    uint32_t width = 800, height = 600;
    uint16_t port = 12345;
    uint16_t rounds_per_sec = 50;
    uint16_t turning_speed = 6;
    uint64_t seed = static_cast<uint64_t >(time(NULL));

    int opt;
    while ((opt = getopt(argc, argv, "W:H:p:s:t:r:")) != -1) {
        if (opt == '?') {
            print_usage();
        }

        std::stringstream ss(optarg);
        switch (opt) {
            case 'W':
                ss >> width;
                break;
            case 'H':
                ss >> height;
                break;
            case 'p':
                ss >> port;
                break;
            case 's':
                ss >> turning_speed;
                break;
            case 'r':
                ss >> seed;
                break;
            default:
                assert(false);
        }
        if (ss.fail()) {
            print_usage();
        }
    }
    set_seed(seed);

    PollWrapper::Ptr client_socket = std::make_shared<PollWrapper>(std::make_unique<UDPSocket>(Address(port)));
    Poller poller;
    poller.addPollee(client_socket);


    GameState game_state(width, height, turning_speed, rounds_per_sec);
    GameServer server(game_state);

    using namespace std::placeholders;

    bool game_ongoing = false;
    std::vector<Event::Ptr> events;
    client_socket->queryReceive(ClientMessage(),
                                std::bind(receive_message, _1, client_socket.get(), &game_state, &server, &events));

    initialize_trigonometry();
    while (true) {
        poller.poll();
        server.purgeClients();
        if (!game_ongoing) {
            server.admitPlayers();
            if (game_state.areAllPlayersReady()) {
                std::cerr << "start game" << std::endl;
                events.clear();
                game_state.begin_game(events);
                server.clear();
                game_state.clear();
                server.setGameId(get_random());
                server.sendEventsToAll(events, *client_socket);
                game_ongoing = true;
            }
        } else {
            if (game_state.update(events)) {
                server.sendEventsToAll(events, *client_socket);
            }
            if (game_state.is_game_over()) {
                game_ongoing = false;
                events.clear();
            }
        }
    }

}