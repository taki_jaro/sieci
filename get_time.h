//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GET_TIME_H
#define ZAD_GET_TIME_H

#include <cstdint>

uint64_t get_current_time_in_us();

#endif //ZAD_GET_TIME_H
