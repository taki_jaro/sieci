//
// Created by jarek on 09.06.17.
//

#include <cmath>
#include "trigonometry.h"

double dx[360];
double dy[360];

void initialize_trigonometry() {
    for (int i = 0; i < 360; ++i) {
        double angle = i / 180.0 * M_PI;
        dx[i] = cos(angle);
        dy[i] = -sin(angle);
    }
}