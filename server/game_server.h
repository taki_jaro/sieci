//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_GAMESERVER_H
#define ZAD_GAMESERVER_H


#include <cstdint>
#include <vector>
#include <map>
#include "../network/address.h"
#include "../network/messages/event.h"
#include "Client.h"
#include "game_state.h"
#include "../network/poll_wrapper.h"

class GameServer {
    std::map <Address, Client> clients;
    GameState &game_state;
    uint32_t game_id;

public:

    GameServer(GameState &game_state);
    void admitPlayers();

    void updateClientStatus(Address address, uint64_t session_id, std::string player_name,
                            uint32_t next_expected_event_no);

    void removeClient(Address address);

    void sendEventsTo(Address address, const std::vector<Event::Ptr> events, PollWrapper &client_socket);

    void sendEventsToAll(const std::vector<Event::Ptr> events, PollWrapper &client_socket);

    void setGameId(uint32_t game_id);

    void clear();

    void purgeClients();
};


#endif //ZAD_GAMESERVER_H
