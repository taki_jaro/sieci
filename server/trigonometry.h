//
// Created by jarek on 09.06.17.
//

#ifndef ZAD_TRIGONOMETRY_H
#define ZAD_TRIGONOMETRY_H

extern double dx[360];
extern double dy[360];

void initialize_trigonometry();

#endif //ZAD_TRIGONOMETRY_H
