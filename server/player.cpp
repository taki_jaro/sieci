//
// Created by jarek on 08.06.17.
//

#include <cmath>
#include "player.h"
#include "trigonometry.h"

Player::Player(const std::string &player_name) : player_name(player_name) {}

bool Player::is_ready() const {
    return ready;
}

void Player::setTurnSpeed(int turn_speed) {
    if (disabled)
        return;
    this->turning_speed = turn_speed;
    if (turn_speed != 0) {
        ready = true;
    }
}

void Player::disable() {
    disabled = true;
}

void Player::setPlayerNo(uint8_t player_no) {
    this->player_no = player_no;
}

uint8_t Player::getPlayerNo() {
    return player_no;
}

void Player::setRandomPos(uint32_t width, uint32_t height) {
    x = rand() % width + 0.5;
    y = rand() % height + 0.5;
    direction = rand() % 360;
}

uint32_t Player::getX() {
    return static_cast<uint32_t >(floor(x));
}

uint32_t Player::getY() {
    return static_cast<uint32_t >(floor(y));
}

bool Player::update() {
    uint32_t old_x = getX();
    uint32_t old_y = getY();
    direction -= turning_speed;
    while (direction < 0) direction += 360;
    direction %= 360;
    x += dx[direction];
    y += dy[direction];

    return getX() != old_x || getY() != old_y;
}


