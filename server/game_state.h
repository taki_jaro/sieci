//
// Created by jarek on 08.06.17.
//

#ifndef ZAD_GAME_STATE_H
#define ZAD_GAME_STATE_H

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <map>
#include "../network/address.h"
#include "../network/messages/event.h"
#include "player.h"

class GameState {
public:
    GameState(uint32_t width, uint32_t height, uint16_t turning_speed, uint16_t rounds_per_sec);

    bool areAllPlayersReady();

    void update_player_move(std::string player_name, int8_t turn_direction);
    void newPlayer(const std::string &player_name);

    void disable_player(const std::string &player_name);
    bool is_game_over();

    void begin_game(std::vector<Event::Ptr> &events);

    bool update(std::vector<Event::Ptr> &events);

    void clear();
private:
    uint32_t width, height;
    uint16_t turning_speed;
    std::map <std::string, Player> players;
    uint64_t accumulated_us;
    uint64_t last_time;
    double update_time;
    std::vector<std::vector<bool> > board;

    void generate_new_game_event(std::vector<Event::Ptr> &events);

    void processPlayer(std::map<std::string, Player>::iterator &it, std::vector<Event::Ptr> &events);
};
#endif //ZAD_GAME_STATE_H
