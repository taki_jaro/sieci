//
// Created by jarek on 08.06.17.
//

#include <cstdint>
#include "Client.h"
#include "../get_time.h"

Client::Client(uint64_t session_id, std::string player_name)
        : session_id(session_id),
          player_name(player_name),
          last_message_time(get_current_time_in_us())
{

}

const std::string &Client::getPlayerName() const {
    return player_name;
}

bool Client::shouldReconnect(uint64_t session_id, const std::string &player_name) const {
    return session_id != this->session_id || player_name != this->player_name;
}

void Client::setExpectedEventNo(uint32_t expected_event_no) {
    this->expected_event_no = expected_event_no;
}

uint32_t Client::getExpectedEventNo() const {
    return expected_event_no;
}

void Client::reset() {
    expected_event_no = 0;
}

void Client::markMessageTime() {
    last_message_time = get_current_time_in_us();
}

bool Client::should_disconnect(uint64_t time) {
    return time - last_message_time > 2*1000*1000;
}

