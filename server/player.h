//
// Created by jarek on 08.06.17.
//

#ifndef ZAD_PLAYER_H
#define ZAD_PLAYER_H

#include <string>

class Player {
public:
    Player(const std::string &player_name);

    bool is_ready() const;
    void setTurnSpeed(int turn_speed);
    void disable();
    void setPlayerNo(uint8_t player_no);
    uint8_t getPlayerNo();
    void setRandomPos(uint32_t width, uint32_t height);
    uint32_t getX();
    uint32_t getY();
    bool update();
private:
    std::string player_name;
    int turning_speed = 0;
    int direction;
    bool ready = false;
    bool disabled = false;
    uint8_t player_no;
    double x, y;
};


#endif //ZAD_PLAYER_H
