//
// Created by jarek on 08.06.17.
//

#include "rand.h"

static uint64_t r;

void set_seed(uint64_t s) {
    r = s;
}

uint32_t get_random() {
    uint64_t ret = r;
    r = (r  * 279470273) % 4294967291;
    return ret;
}
