//
// Created by jarek on 08.06.17.
//

#ifndef ZAD_CLIENT_H
#define ZAD_CLIENT_H

#include <string>

class Client {
public:
    Client(uint64_t session_id, std::string player_name);

    const std::string &getPlayerName() const;
    bool shouldReconnect(uint64_t session_id, const std::string &player_name) const;
    void setExpectedEventNo(uint32_t expected_event_no);
    uint32_t getExpectedEventNo() const;
    void reset();
    void markMessageTime();
    bool should_disconnect(uint64_t time);
private:
    const uint64_t session_id;
    const std::string player_name;
    uint32_t expected_event_no = 0;
    uint64_t last_message_time;
};


#endif //ZAD_CLIENT_H
