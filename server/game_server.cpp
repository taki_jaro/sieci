//
// Created by jarek on 09.06.17.
//

#include "game_state.h"
#include <algorithm>
#include <iostream>
#include "game_server.h"
#include "../network/poll_wrapper.h"
#include "../network/messages/server_message.h"
#include "../consts.h"
#include "../get_time.h"

GameServer::GameServer(GameState &game_state) : game_state(game_state) {

}

void GameServer::admitPlayers() {
    for (const auto &p : clients) {
        const auto &client = p.second;
        const auto &player_name = client.getPlayerName();
        if (player_name != "") game_state.newPlayer(player_name);
    }
}

void GameServer::updateClientStatus(Address address, uint64_t session_id, std::string player_name,
                                    uint32_t next_expected_event_no) {
    if (!clients.count(address)) {
        clients.insert({address, Client(session_id, player_name)});
    }

    if (clients.find(address)->second.shouldReconnect(session_id, player_name)) {
        removeClient(address);
        clients.insert({address, Client(session_id, player_name)});
    }

    clients.find(address)->second.setExpectedEventNo(next_expected_event_no);
    clients.find(address)->second.markMessageTime();
}

void GameServer::removeClient(Address address) {
    const Client &client = clients.find(address)->second;
    game_state.disable_player(client.getPlayerName());
    clients.erase(address);
}

void GameServer::sendEventsTo(Address address, const std::vector<Event::Ptr> events, PollWrapper &client_socket) {
    if (!clients.count(address)) {
        return;
    }

    const auto &client = clients.find(address)->second;

    if (client.getExpectedEventNo() >= events.size())
        return;
    auto it = events.begin() + client.getExpectedEventNo();
    while (it != events.end()) {
        std::unique_ptr<ServerMessage> message = std::make_unique<ServerMessage>();
        message->game_id = game_id;
        size_t size = 4;
        while (it != events.end() && size + (*it)->getEventLength() < MAX_PACKET_SIZE) {
            message->events.push_back(*it);
            size += (*it)->getEventLength() + 8;
            ++it;
        }
        if (!message->events.empty())
            client_socket.querySend(address, std::move(message));
    }
}

void GameServer::sendEventsToAll(const std::vector<Event::Ptr> events, PollWrapper &client_socket) {
    for (const auto &p : clients) {
        sendEventsTo(p.first, events, client_socket);
    }
}

void GameServer::setGameId(uint32_t game_id) {
    this->game_id = game_id;
}

void GameServer::clear() {
    for (auto &p : clients) {
        Client &client = p.second;
        client.reset();
    }
}

void GameServer::purgeClients() {
    uint64_t time = get_current_time_in_us();
    auto it = clients.begin();
    while (it != clients.end()) {
        if (it->second.should_disconnect(time)) {
            std::cerr << "purging" << std::endl;
            it = clients.erase(it);
        } else {
            ++it;
        }
    }
}
