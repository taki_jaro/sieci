//
// Created by jarek on 08.06.17.
//

#ifndef ZAD_RAND_H
#define ZAD_RAND_H

#include <cstdint>

void set_seed(uint64_t s);
uint32_t get_random();

#endif //ZAD_RAND_H
