//
// Created by jarek on 08.06.17.
//

#include <algorithm>
#include <sys/time.h>
#include <cmath>
#include <iostream>
#include "game_state.h"
#include "game_server.h"
#include "../network/messages/events/new_game_event.h"
#include "../network/messages/events/player_eliminated_event.h"
#include "../network/messages/events/pixel_event.h"
#include "../get_time.h"
#include "../network/messages/events/game_over_event.h"

void GameState::newPlayer(const std::string &player_name) {
    if (!players.count(player_name))
        players.insert({player_name, Player(player_name)});
}

GameState::GameState(uint32_t width, uint32_t height, uint16_t turning_speed, uint16_t rounds_per_sec)
        : width(width),
          height(height),
          turning_speed(turning_speed),
          update_time(1e6 / rounds_per_sec) {
    board.resize(height);
    for (auto &row : board) {
        row.resize(width, false);
    }
}

bool GameState::areAllPlayersReady() {
    if (players.size() < 2) {
        return false;
    }
    for (const auto &p : players) {
        const Player &player = p.second;
        if (!player.is_ready()) {
            return false;
        }
    }
    return true;
}

void GameState::update_player_move(std::string player_name, int8_t turn_direction) {
    if (!players.count(player_name)) {
        return;
    }

    players.at(player_name).setTurnSpeed(turn_direction * turning_speed);
}

bool GameState::is_game_over() {
    return players.size() <= 1;
}

void GameState::disable_player(const std::string &player_name) {
    players.find(player_name)->second.disable();
}

void GameState::begin_game(std::vector<Event::Ptr> &events) {
    generate_new_game_event(events);

    accumulated_us = 0;
    last_time = get_current_time_in_us();
    auto it = players.begin();
    uint8_t player_no = 0;
    while (it != players.end()) {
        Player &player = it->second;
        player.setPlayerNo(player_no);
        player_no++;

        player.setRandomPos(width, height);
        processPlayer(it, events);
    }
}

void GameState::generate_new_game_event(std::vector<Event::Ptr> &events) {
    auto event = std::make_shared<NewGameEvent>();
    event->maxx = width;
    event->maxy = height;
    size_t size = event->getEventLength();
    for (auto it = players.begin(); it != players.end();) {
        if (size + it->first.size() + 1) {
            size += it->first.size();
            event->player_names.push_back(it->first);
            ++it;
        } else {
            it = players.erase(it);
        }
    }
    event->event_no = 0;
    events.push_back(move(event));
}



bool GameState::update(std::vector<Event::Ptr> &events) {
    bool modified = false;

    uint64_t current_time = get_current_time_in_us();
    accumulated_us += current_time - last_time;
    last_time = current_time;

    while (accumulated_us > update_time) {
        std::cout << "do_update" << std::endl;
        accumulated_us -= floor(update_time);
        auto it = players.begin();
        while (it != players.end()) {
            Player &player = it->second;
            bool moved;
            moved = player.update();
            if (moved) {
                modified = true;
                processPlayer(it, events);
            }
        }
    }
//    if (is_game_over()) {
//        events.push_back(std::make_shared<GameOverEvent>());
//    }
}

void GameState::processPlayer(std::map<std::string, Player>::iterator &it, std::vector<Event::Ptr> &events) {
    Player &player = it->second;
    int32_t px = player.getX(), py = player.getY();
    if (px < 0 || px >= width || py < 0 || py >= height || board[py][px]) {
        std::shared_ptr<PlayerEliminatedEvent> event = std::make_shared<PlayerEliminatedEvent>();
        event->player_number = player.getPlayerNo();
        event->event_no = events.size();
        events.push_back(event);
        it = players.erase(it);
        std::cout << "Player eliminated" << std::endl;
    } else {
        if (0 <= px && px < width && 0 <= py && py < height)
            board[py][px] = true;
        std::shared_ptr<PixelEvent> event = std::make_shared<PixelEvent>();
        event->x = static_cast<uint32_t>(px);
        event->y = static_cast<uint32_t>(py);
        event->player_number = player.getPlayerNo();
        event->event_no = events.size();
        events.push_back(event);
        ++it;
        std::cout << "Pixel" << std::endl;
    }
}

void GameState::clear() {
    board.clear();
    board.resize(height);
    for (auto &row : board) {
        row.resize(width, false);
    }
}

