#include <iostream>
#include "network/message.h"
#include "network/udp_socket.h"
#include "network/poll_wrapper.h"
#include "network/poller.h"
#include "network/messages/server_message.h"
#include "network/messages/events/new_game_event.h"
#include "network/messages/events/player_eliminated_event.h"
#include "network/messages/events/pixel_event.h"
#include "network/messages/events/game_over_event.h"

class TestMessage : public Message {
public:
    void parse(const std::vector<uint8_t> &data) override {
        printf("%d\n", data == std::vector<uint8_t>{'t', 'e', 's', 't','\n'});
    }

    std::vector<uint8_t> toData() const override {
        return {'t', 'e', 's', 't'};
    }
};

//int main() {
//    Address server_address(12345), client_address(12346);
//    std::unique_ptr<Socket> server_socket = std::make_unique<UDPSocket>(server_address);
//    std::unique_ptr<Socket> client_socket = std::make_unique<UDPSocket>(client_address);
//    PollWrapper::Ptr serverPollWrapper = std::make_shared<PollWrapper>(std::move(server_socket));
//    PollWrapper::Ptr clientPollWrapper = std::make_shared<PollWrapper>(std::move(client_socket));
//
//    ServerMessage message;
//    message.game_id = 123456789;
//    auto new_game = std::make_shared<NewGameEvent>();
//    new_game->maxx = 65000;
//    new_game->maxy = 64000;
//    new_game->player_names.push_back("test1");
//    new_game->player_names.push_back("test2");
//    auto player_eliminated = std::make_shared<PlayerEliminatedEvent>();
//    player_eliminated->player_number = 0;
//    auto pixel = std::make_shared<PixelEvent>();
//    pixel->x = 60000;
//    pixel->y = 65000;
//    auto game_over = std::make_shared<GameOverEvent>();
//    message.events.push_back(new_game);
//    message.events.push_back(player_eliminated);
//    message.events.push_back(pixel);
//    message.events.push_back(game_over);
//    serverPollWrapper->querySend({0x7F000001, 12346}, message);
//
//    clientPollWrapper->queryReceive(message, [&](Message &msg, PollWrapper &) {
//        ServerMessage &parsedMessage = (ServerMessage &) msg;
//        if (parsedMessage.game_id != 123456789) {
//            printf("Error game_id");
//        }
//        if (parsedMessage.events.size() != 4) {
//            printf("Error size");
//        }
//        Event::Ptr event;
//        event = parsedMessage.events[0];
//        if (event->event_type != NewGameEvent::EVENT_TYPE) {
//            printf("Error event_type 1");
//        }
//        NewGameEvent *n = (NewGameEvent *) event.get();
//        if (n->maxx != new_game->maxx || n->maxy != new_game->maxy || n->player_names != new_game->player_names) {
//            printf("Error new game");
//        }
//
//        event = parsedMessage.events[1];
//        if (event->event_type != PlayerEliminatedEvent::EVENT_TYPE) {
//            printf("Error event_type 2");
//        }
//        PlayerEliminatedEvent *p = (PlayerEliminatedEvent *) event.get();
//        if (p->player_number != player_eliminated->player_number) {
//            printf("Error player eliminated");
//        }
//
//        event = parsedMessage.events[2];
//        if (event->event_type != PixelEvent::EVENT_TYPE) {
//            printf("Error event_type 3");
//        }
//        PixelEvent *px = (PixelEvent *) event.get();
//        if (px->x != pixel->x || px->y != pixel->y) {
//            printf("Error pixel");
//        }
//
//        event = parsedMessage.events[3];
//        if (event->event_type != GameOverEvent::EVENT_TYPE) {
//            printf("Error event_type 1");
//        }
//        printf("Done\n");
//    });
//
//    Poller poller;
//    poller.addPollee(serverPollWrapper);
//    poller.addPollee(clientPollWrapper);
//    while (true) {
//        poller.poll();
//
//    }
//}